package org.advadmin;

import org.bukkit.Color;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private static Main main;

    public static final String PREFIX = Color.GREEN + "[Advanced Admin] ";

    @Override
    public void onLoad() {
        this.getLogger().info(PREFIX + "Loading plugin...");
        main = this;
    }

    @Override
    public void onEnable() {
        this.getLogger().info(PREFIX + "Enabling plugin...");
    }

    @Override
    public void onDisable() {
        this.getLogger().info(PREFIX + "Disabling plugin...");
    }

    public static Main getInstance(){
        return main;
    }
}
