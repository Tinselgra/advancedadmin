package org.advadmin.modules.Economy;

import org.advadmin.Main;
import org.bukkit.plugin.RegisteredServiceProvider;

public class Economy {

    public Economy(){

    }

    public void start(){
        if (getPlugin().getServer().getPluginManager().getPlugin("Vault") == null){
            getPlugin().getServer().getPluginManager().disablePlugin(getPlugin());
            return;
        }
        RegisteredServiceProvider<net.milkbowl.vault.economy.Economy> serviceProvider = getPlugin().getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (serviceProvider == null){
            getPlugin().getServer().getPluginManager().disablePlugin(getPlugin());
            return;
        }
        getPlugin().getLogger().info(Main.PREFIX + "Economy feature loaded!");
    }

    public Main getPlugin(){
        return Main.getInstance();
    }
}
